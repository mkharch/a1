
PROJ_CODE := a1

.PHONY: default

default: vmspin

DRAGONEGG := ../dragonegg/dragonegg.so

CFLAGS := -I include
CFLAGS += -D__XEN_INTERFACE_VERSION__=0x00030205

LDFLAGS := -T spin.lds -nostdlib
LDFLAGS += -Xlinker -Map=spin.map

OBJS := startup.o spin.o xencons.o hello.o stub.o

vmspin: $(OBJS)
	gcc -o $@ $(LDFLAGS) $^
	rsync vmspin dom0::images/go-$(PROJ_CODE).img

startup.o: startup.S
	gcc -c $(CFLAGS) -o $@ $<

xencons.o: xencons.c
	gcc -c $(CFLAGS) -o $@ $<

spin.o: spin.c
	gcc -c $(CFLAGS) -o $@ $<

hello.ll: hello.go
	gccgo -S -c -o $@ -fplugin=$(DRAGONEGG) -fplugin-arg-dragonegg-emit-ir $<

hello_x86.s: hello.ll
	llc -march=x86-64 -o $@ $<

hello.o: hello_x86.s
	gcc -c -o $@ $<

stub.o: stub.c
	gcc -c -o $@ $<

