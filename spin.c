//
//
//

#include "spin.h"

#include <stdint.h>

#include "xencons.h"

#include "hypercall-x86_64.h"

uint8_t stack[2 *STACK_SIZE];

start_info_t start_info;
unsigned long *phys_to_machine_mapping;

void hello_main(void) __asm__ ("main.main");

void start_spin(start_info_t *si)
{
	uint8_t *src = (uint8_t *)si;
	uint8_t *dst = (uint8_t *)&start_info;
	int n = sizeof(start_info);
	while (n-- > 0)
		*dst++ = *src++;

	phys_to_machine_mapping = (unsigned long *)start_info.mfn_list;

	xencons_init(mfn_to_virt(start_info.console.domU.mfn),
							start_info.console.domU.evtchn);

	hello_main();	//Go!

	//xencons_write("Hey there, cowboy\n", 18);

	//HYPERVISOR_console_io(CONSOLEIO_write, 18, "Hey there, cowboy\n");

	//struct sched_shutdown op;
	//op.reason = SHUTDOWN_poweroff;
	//HYPERVISOR_sched_op(SCHEDOP_shutdown, &op);
	
	while (1);
}

static int zero = 0;

void crash()
{
	stack[0] /= zero;
}

void crash1(int a)
{
	stack[0] /= zero;
}

void crash2(int a, int b)
{
	stack[0] /= zero;
}

//EOF
