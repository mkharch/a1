//
//
//

#include <stdint.h>

#include "spin.h"

#include "xen/io/console.h"

#include "hypercall-x86_64.h"

static struct xencons_interface *_iface;
static uint32_t _evtchn;

static xencons_int(uint32_t evtchn, void *data);

void xencons_init(struct xencons_interface *iface, uint32_t evtchn)
{
	_iface = iface;
	_evtchn = evtchn;

	//event_bind(evtchn, xencons_int, 0);
}

void xencons_write(const char *str, int len)
{
	int sent = 0;

	while (sent < len)
	{
		XENCONS_RING_IDX cons, prod;
		cons = _iface->out_cons;
		rmb();
		prod = _iface->out_prod;

		while ((sent < len) && (prod -cons < sizeof(_iface->out)))
		{
			int i = MASK_XENCONS_IDX(prod++, _iface->out);
			_iface->out[i] = str[sent++];
		}

		_iface->out_prod = prod;
		wmb();

		// event_kick
		evtchn_send_t op;
		op.port = _evtchn;
		HYPERVISOR_event_channel_op(EVTCHNOP_send, &op);
	}
}

static xencons_int(uint32_t evtchn, void *data)
{
	//TODO
}

//TODO

//EOF
