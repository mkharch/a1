	.file	"hello.ll"
                                        # Start of file scope inline assembly
	.ident	"GCC: (GNU) 4.7.2 20121109 (Red Hat 4.7.2-8) LLVM: 3.2svn"

                                        # End of file scope inline assembly
	.text
	.globl	main.main
	.align	16, 0x90
	.type	main.main,@function
main.main:                              # @main.main
	.cfi_startproc
	.cfi_personality 3, __gccgo_personality_v0
.Leh_func_begin0:
	.cfi_lsda 3, .Lexception0
# BB#0:                                 # %2
	subq	$72, %rsp
.Ltmp4:
	.cfi_def_cfa_offset 80
	movl	$19, %edi
	callq	__go_new_nopointers
	movb	$72, (%rax)
	movb	$101, 1(%rax)
	movb	$121, 2(%rax)
	movb	$32, 3(%rax)
	movb	$116, 4(%rax)
	leaq	32(%rsp), %rdi
	movb	$104, 5(%rax)
	movb	$101, 6(%rax)
	movb	$114, 7(%rax)
	movb	$101, 8(%rax)
	movb	$44, 9(%rax)
	movb	$32, 10(%rax)
	movb	$99, 11(%rax)
	movb	$111, 12(%rax)
	movb	$119, 13(%rax)
	movb	$98, 14(%rax)
	movb	$111, 15(%rax)
	movb	$121, 16(%rax)
	movb	$13, 17(%rax)
	movb	$10, 18(%rax)
	movq	%rax, 56(%rsp)
	movl	$19, 64(%rsp)
	movl	$19, 68(%rsp)
	movq	56(%rsp), %rdx
	movq	64(%rsp), %rcx
	movl	syscall.Stdout(%rip), %esi
.Ltmp0:
	callq	syscall.Write
.Ltmp1:
# BB#1:                                 # %return
	movq	40(%rsp), %rax
	movq	%rax, 16(%rsp)
	movq	48(%rsp), %rax
	movq	%rax, 24(%rsp)
	addq	$72, %rsp
	ret
.LBB0_2:                                # %4
.Ltmp2:
	movq	%rax, 8(%rsp)
	movl	%edx, 4(%rsp)
	movq	8(%rsp), %rdi
	callq	_Unwind_Resume
.Ltmp5:
	.size	main.main, .Ltmp5-main.main
	.cfi_endproc
.Leh_func_end0:
	.section	.gcc_except_table,"a",@progbits
	.align	4
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	 "\234"                 # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
.Lset0 = .Ltmp0-.Leh_func_begin0        # >> Call Site 1 <<
	.long	.Lset0
.Lset1 = .Ltmp1-.Ltmp0                  #   Call between .Ltmp0 and .Ltmp1
	.long	.Lset1
.Lset2 = .Ltmp2-.Leh_func_begin0        #     jumps to .Ltmp2
	.long	.Lset2
	.byte	0                       #   On action: cleanup
.Lset3 = .Ltmp1-.Leh_func_begin0        # >> Call Site 2 <<
	.long	.Lset3
.Lset4 = .Leh_func_end0-.Ltmp1          #   Call between .Ltmp1 and .Leh_func_end0
	.long	.Lset4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.align	4

	.text
	.globl	__go_init_main
	.align	16, 0x90
	.type	__go_init_main,@function
__go_init_main:                         # @__go_init_main
	.cfi_startproc
# BB#0:                                 # %entry
	pushq	%rax
.Ltmp7:
	.cfi_def_cfa_offset 16
	callq	runtime..import
	callq	syscall..import
	popq	%rax
	ret
.Ltmp8:
	.size	__go_init_main, .Ltmp8-__go_init_main
	.cfi_endproc


	.section	".note.GNU-stack","",@progbits
