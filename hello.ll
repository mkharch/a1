; ModuleID = 'hello.go'
target datalayout = "e-p:64:64:64-S128-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f16:16:16-f32:32:32-f64:64:64-f128:128:128-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64"
target triple = "x86_64-redhat-linux"

module asm "\09.ident\09\22GCC: (GNU) 4.7.2 20121109 (Red Hat 4.7.2-8) LLVM: 3.2svn\22"

%0 = type { i8*, i32, i32 }
%1 = type { i32, %struct.error }
%2 = type { %struct.commonType*, i8* }
%struct.error = type { %2*, i8* }
%struct.commonType = type { i8, i8, i8, i64, i32, i8*, i8*, i8*, i8*, i8* }

@syscall.Stdout = external global i32

define void @main.main() unnamed_addr uwtable {
entry:
  %s = alloca %0
  %D.723 = alloca %1
  %GOTMP.2 = alloca %struct.error
  %exc_tmp = alloca i8*
  %filt_tmp = alloca i32
  %"alloca point" = bitcast i32 0 to i32
  %"ssa point" = bitcast i32 0 to i32
  br label %"2"

"2":                                              ; preds = %entry
  %0 = call i8* @__go_new_nopointers(i64 19) nounwind
  %1 = bitcast i8* %0 to [19 x i8]*
  %2 = bitcast [19 x i8]* %1 to i8*
  %3 = getelementptr i8* %2, i64 0
  store i8 72, i8* %3, align 1
  %4 = bitcast i8* %0 to [19 x i8]*
  %5 = bitcast [19 x i8]* %4 to i8*
  %6 = getelementptr i8* %5, i64 1
  store i8 101, i8* %6, align 1
  %7 = bitcast i8* %0 to [19 x i8]*
  %8 = bitcast [19 x i8]* %7 to i8*
  %9 = getelementptr i8* %8, i64 2
  store i8 121, i8* %9, align 1
  %10 = bitcast i8* %0 to [19 x i8]*
  %11 = bitcast [19 x i8]* %10 to i8*
  %12 = getelementptr i8* %11, i64 3
  store i8 32, i8* %12, align 1
  %13 = bitcast i8* %0 to [19 x i8]*
  %14 = bitcast [19 x i8]* %13 to i8*
  %15 = getelementptr i8* %14, i64 4
  store i8 116, i8* %15, align 1
  %16 = bitcast i8* %0 to [19 x i8]*
  %17 = bitcast [19 x i8]* %16 to i8*
  %18 = getelementptr i8* %17, i64 5
  store i8 104, i8* %18, align 1
  %19 = bitcast i8* %0 to [19 x i8]*
  %20 = bitcast [19 x i8]* %19 to i8*
  %21 = getelementptr i8* %20, i64 6
  store i8 101, i8* %21, align 1
  %22 = bitcast i8* %0 to [19 x i8]*
  %23 = bitcast [19 x i8]* %22 to i8*
  %24 = getelementptr i8* %23, i64 7
  store i8 114, i8* %24, align 1
  %25 = bitcast i8* %0 to [19 x i8]*
  %26 = bitcast [19 x i8]* %25 to i8*
  %27 = getelementptr i8* %26, i64 8
  store i8 101, i8* %27, align 1
  %28 = bitcast i8* %0 to [19 x i8]*
  %29 = bitcast [19 x i8]* %28 to i8*
  %30 = getelementptr i8* %29, i64 9
  store i8 44, i8* %30, align 1
  %31 = bitcast i8* %0 to [19 x i8]*
  %32 = bitcast [19 x i8]* %31 to i8*
  %33 = getelementptr i8* %32, i64 10
  store i8 32, i8* %33, align 1
  %34 = bitcast i8* %0 to [19 x i8]*
  %35 = bitcast [19 x i8]* %34 to i8*
  %36 = getelementptr i8* %35, i64 11
  store i8 99, i8* %36, align 1
  %37 = bitcast i8* %0 to [19 x i8]*
  %38 = bitcast [19 x i8]* %37 to i8*
  %39 = getelementptr i8* %38, i64 12
  store i8 111, i8* %39, align 1
  %40 = bitcast i8* %0 to [19 x i8]*
  %41 = bitcast [19 x i8]* %40 to i8*
  %42 = getelementptr i8* %41, i64 13
  store i8 119, i8* %42, align 1
  %43 = bitcast i8* %0 to [19 x i8]*
  %44 = bitcast [19 x i8]* %43 to i8*
  %45 = getelementptr i8* %44, i64 14
  store i8 98, i8* %45, align 1
  %46 = bitcast i8* %0 to [19 x i8]*
  %47 = bitcast [19 x i8]* %46 to i8*
  %48 = getelementptr i8* %47, i64 15
  store i8 111, i8* %48, align 1
  %49 = bitcast i8* %0 to [19 x i8]*
  %50 = bitcast [19 x i8]* %49 to i8*
  %51 = getelementptr i8* %50, i64 16
  store i8 121, i8* %51, align 1
  %52 = bitcast i8* %0 to [19 x i8]*
  %53 = bitcast [19 x i8]* %52 to i8*
  %54 = getelementptr i8* %53, i64 17
  store i8 13, i8* %54, align 1
  %55 = bitcast i8* %0 to [19 x i8]*
  %56 = bitcast [19 x i8]* %55 to i8*
  %57 = getelementptr i8* %56, i64 18
  store i8 10, i8* %57, align 1
  %58 = getelementptr inbounds %0* %s, i32 0, i32 0
  store i8* %0, i8** %58, align 8
  %59 = getelementptr inbounds %0* %s, i32 0, i32 1
  store i32 19, i32* %59, align 8
  %60 = getelementptr inbounds %0* %s, i32 0, i32 2
  store i32 19, i32* %60, align 4
  %61 = load i32* @syscall.Stdout, align 4
  %62 = bitcast %0* %s to { i64, i64 }*
  %63 = getelementptr inbounds { i64, i64 }* %62, i32 0, i32 0
  %val = load i64* %63, align 1
  %64 = bitcast %0* %s to { i64, i64 }*
  %65 = getelementptr inbounds { i64, i64 }* %64, i32 0, i32 1
  %val1 = load i64* %65, align 1
  invoke void @syscall.Write(%1* noalias sret %D.723, i32 %61, i64 %val, i64 %val1)
          to label %66 unwind label %"4"

; <label>:66                                      ; preds = %"2"
  br label %"3"

"3":                                              ; preds = %66
  %67 = getelementptr inbounds %1* %D.723, i32 0, i32 0
  %68 = load i32* %67, align 8
  %69 = getelementptr inbounds %1* %D.723, i32 0, i32 1
  %70 = getelementptr inbounds %struct.error* %GOTMP.2, i32 0, i32 0
  %71 = getelementptr inbounds %struct.error* %69, i32 0, i32 0
  %72 = load %2** %71, align 8
  store %2* %72, %2** %70, align 8
  %73 = getelementptr inbounds %struct.error* %GOTMP.2, i32 0, i32 1
  %74 = getelementptr inbounds %struct.error* %69, i32 0, i32 1
  %75 = load i8** %74, align 8
  store i8* %75, i8** %73, align 8
  %76 = bitcast %0* %s to i8*
  call void @llvm.lifetime.end(i64 16, i8* %76)
  br label %return

"4":                                              ; preds = %"2"
  %77 = landingpad { i8*, i32 } personality i32 (i32, i64, i8*, i8*)* @__gccgo_personality_v0
          cleanup
  %exc_ptr2 = extractvalue { i8*, i32 } %77, 0
  store i8* %exc_ptr2, i8** %exc_tmp
  %filter3 = extractvalue { i8*, i32 } %77, 1
  store i32 %filter3, i32* %filt_tmp
  %78 = bitcast %0* %s to i8*
  call void @llvm.lifetime.end(i64 16, i8* %78)
  %79 = load i8** %exc_tmp
  %80 = load i32* %filt_tmp
  %exc_ptr = insertvalue { i8*, i32 } undef, i8* %79, 0
  %filter = insertvalue { i8*, i32 } %exc_ptr, i32 %80, 1
  resume { i8*, i32 } %filter

return:                                           ; preds = %"3"
  ret void
}

declare i8* @__go_new_nopointers(i64) nounwind

declare void @syscall.Write(%1* noalias sret, i32, i64, i64)

declare void @llvm.lifetime.end(i64, i8* nocapture) nounwind

declare i32 @__gccgo_personality_v0(i32, i64, i8*, i8*)

define void @__go_init_main() unnamed_addr uwtable {
entry:
  %"ssa point" = bitcast i32 0 to i32
  br label %"2"

"2":                                              ; preds = %entry
  call void @runtime..import()
  call void @syscall..import()
  br label %return

return:                                           ; preds = %"2"
  ret void
}

declare void @runtime..import()

declare void @syscall..import()
