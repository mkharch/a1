//
//
//

#include <stdint.h>

int32_t syscall_Stdout __asm__("syscall.Stdout");
int32_t syscall_Stdout = 0;

void syscall_Write(void *p, int32_t a, int64_t b, int64_t c) __asm__("syscall.Write");
void runtime__import(void) __asm__("runtime..import");
void syscall__import(void) __asm__("syscall..import");


int8_t _new_noptr_buf[4096];

int8_t *__go_new_nopointers(int64_t sz)
{
	return _new_noptr_buf;
}

void syscall_Write(void *p, int32_t a, int64_t b, int64_t c)
{
	const char *str = (const char *)b;
	int size = (int)c;
	xencons_write(str, size);
}

void runtime__import(void)
{
}

void syscall__import(void)
{
}

int32_t __gccgo_personality_v0(int32_t a, int64_t b, int8_t *c, int8_t *d)
{
}

void _Unwind_Resume(void)
{
}

// hello.o:     file format elf64-x86-64
// 
// SYMBOL TABLE:
// 0000000000000000 l    df *ABS*	0000000000000000 hello.ll
// 0000000000000000 l    d  .text	0000000000000000 .text
// 0000000000000000 l    d  .data	0000000000000000 .data
// 0000000000000000 l    d  .bss	0000000000000000 .bss
// 0000000000000000 l    d  .note.GNU-stack	0000000000000000 .note.GNU-stack
// 0000000000000000 l    d  .eh_frame	0000000000000000 .eh_frame
// 0000000000000000 l    d  .comment	0000000000000000 .comment
// 0000000000000000 g     F .text	000000000000006c main.main
// 0000000000000000         *UND*	0000000000000000 __go_new_nopointers
// 0000000000000000         *UND*	0000000000000000 syscall.Stdout
// 0000000000000000         *UND*	0000000000000000 syscall.Write
// 0000000000000070 g     F .text	000000000000000d __go_init_main
// 0000000000000000         *UND*	0000000000000000 runtime..import
// 0000000000000000         *UND*	0000000000000000 syscall..import
// 

// declare i8* @__go_new_nopointers(i64) nounwind
// declare void @syscall.Write(%1* noalias sret, i32, i64, i64)
// declare void @runtime..import()
// declare void @syscall..import()
// declare i32 @__gccgo_personality_v0(i32, i64, i8*, i8*)
//
