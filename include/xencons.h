#pragma once

#include <xen/io/console.h>

void xencons_init(struct xencons_interface *iface, uint32_t evtchn);
void xencons_write(const char *str, int len);

//EOF
