#pragma once

#define STACK_SIZE	65536

#define PAGE_SIZE	4096
#define PAGE_SHIFT	12

#ifndef __ASSEMBLY__
#include <stdint.h>
#include <xen/xen.h>

extern shared_info_t	shared_info;

typedef struct { unsigned long pte; } pte_t;
#define __pte(x) ((pte_t) { (x) } )

#define mb()	__asm__ __volatile__ ("mfence":::"memory")
#define rmb()	__asm__ __volatile__ ("lfence":::"memory")
#define wmb()	__asm__ __volatile__ ("sfence":::"memory")

#define VIRT_START                 ((unsigned long)0)
#define to_virt(x)                 ((void *)((unsigned long)(x)+VIRT_START))
#define mfn_to_pfn(_mfn)		(machine_to_phys_mapping[(_mfn)])
#define mfn_to_virt(_mfn)          (to_virt(mfn_to_pfn(_mfn) << PAGE_SHIFT))

void crash();
void crash1(int);
void crash2(int, int);
#endif

